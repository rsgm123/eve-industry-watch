import csv
import os
import requests

# 'system',
# 'cost_value',
# 'cost_change',
# 'sov_alliance_id',
# 'sov_coalition_name',
# 'alliance_members',
# 'coalition_members'
from collections import defaultdict

from builtins import type

systems = defaultdict(lambda: 0)

for file in os.listdir('/tmp/eve-test/'):
    with open('/tmp/eve-test/' + file, mode='r') as f:
        reader = csv.reader(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        next(reader, None)

        for (
                system,
                cost_value,
                cost_change,
                sov_alliance_id,
                sov_coalition_name,
                alliance_members,
                coalition_members
        ) in reader:
            change = float(cost_change)
            if change > 0.0:
                systems[system] += change

base_url = 'https://esi.evetech.net'
def get_system_name(system):
    return requests.get(base_url + '/v4/universe/systems/' + system).json()['name']


print(len(systems))
[print('{}:  {}'.format(get_system_name(system), value)) for system, value in
 sorted(systems.items(), key=lambda x: x[1], reverse=True)[:50]]
