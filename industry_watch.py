#!/bin/python3
import csv
import json
from time import sleep

import datetime
import requests
from collections import defaultdict

import time
from requests.exceptions import HTTPError

base_url = 'https://esi.evetech.net'

# daily caches
last_cache_update = datetime.datetime.now()
sov_cache = {}  # system_id -> alliance_id
coalition_dict_cache = {}  # alliance_id -> (member_count, coalition name, coalition_members)
alliance_members_cache = {}  # alliance_id -> member_count

system_costs = {}


"""
example csv data for date

system,cost value,cost change,sov alliance id,alliance members,sov coalition name,coalition members,
"""


def clear_caches():
    global sov_cache
    global coalition_dict_cache
    global alliance_members_cache

    # rebuild sov data
    sov_data = requests.get(base_url + '/v1/sovereignty/map/')
    try:
        sov_data.raise_for_status()
    except HTTPError:
        pass
    else:
        sov_cache = {x['system_id']: x['alliance_id'] for x in sov_data.json() if 'alliance_id' in x}

    # rebuild coalition data
    coalition_data = requests.get('http://rischwa.net/api/coalitions/current')
    try:
        coalition_data.raise_for_status()
    except HTTPError:
        pass
    else:
        coalition_dict_cache = {}
        for coalition in coalition_data.json()['coalitions']:
            for alliance in coalition['alliances']:
                coalition_dict_cache[alliance['id']] = (
                    alliance['memberCount'],
                    coalition['name'],
                    coalition['memberCount']
                )

    alliance_members_cache = {}


# evewho.com/api broke at some point, rip
# def get_alliance_members(alliance_id):
#     global alliance_members_cache
#     if alliance_id in alliance_members_cache:
#         return alliance_members_cache[alliance_id]
# 
#     response = requests.get(f'https://evewho.com/api.php?type=alliance&id={alliance_id}')
#     try:
#         response.raise_for_status()
#     except HTTPError:
#         return 0
# 
#     members = response.json()['info']['member_count']
#     alliance_members_cache[alliance_id] = members
#     return members


def get_sov(system):
    """
    Return sov information on a system.
    Information includes sov holding alliance ticker and coalition
    """
    alliance_id = sov_cache.get(system, None)

    if not alliance_id:
        return 0, 0, '', 0

    alliance_members, coalition_name, coalition_members = coalition_dict_cache.get(alliance_id, (None, '', 0))

    if not alliance_members:  # (not in coalition)
        alliance_members = 0  # get_alliance_members(alliance_id)

    return alliance_id, alliance_members, coalition_name, coalition_members


def get_system_info(system):
    global system_costs

    system_id = system['solar_system_id']
    cost = [c['cost_index'] for c in system['cost_indices'] if c['activity'] == 'manufacturing'][0]

    cost_change = cost - system_costs[system_id] if system_id in system_costs else 0

    system_costs[system_id] = cost

    # system,cost value,cost change,sov alliance id,sov coalition name,alliance members,coalition members,
    alliance_id, alliance_members, coalition_name, coalition_members = get_sov(system_id)

    return system_id, cost, cost_change, alliance_id, alliance_members, coalition_name, coalition_members


def write_csv(datetime, data):
    with open(f'data/{datetime}.csv', mode='w') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow([
            'system',
            'cost_value',
            'cost_change',
            'sov_alliance_id',
            'sov_coalition_name',
            'alliance_members',
            'coalition_members'
        ])
        for row in data:
            writer.writerow(row)


def run():
    while True:
        now = datetime.datetime.now()
        a = time.time()
        data = []

        if now - last_cache_update > datetime.timedelta(days=1):
            clear_caches()

        response = requests.get(base_url + '/v1/industry/systems/')
        try:
            response.raise_for_status()
        except HTTPError:
            sleep(3602)
            continue

        for system in response.json():
            info = get_system_info(system)
            data.append(info)

        write_csv(now, data)

        sleep(3602)


clear_caches()
run()
