FROM python:3-stretch

ENV PYTHONUNBUFFERED 1
ENV LANG C.UTF-8


# Install dependencies and setup user
RUN mkdir /app


# Install python requirements and gunicorn
COPY . /app
RUN cd /app \
    && pip3 install -r /app/requirements.txt


WORKDIR /app
CMD ["python3", "/app/industry_watch.py"]

